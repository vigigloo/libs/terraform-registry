use super::Address;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
    #[error(transparent)]
    ParseUrl(#[from] url::ParseError),

    #[error("{msg}")]
    Http {
        msg: String,
        #[source]
        err: reqwest::Error,
    },

    #[error("Missing address parts ({}): `{address}`", parts.join(", "))]
    MissingAddressParts { address: String, parts: Vec<String> },

    #[error("No endpoint available: {0}")]
    NoEndpointAvailable(String),

    #[error("Invalid address: {0}")]
    InvalidAddress(Address),
}

#[allow(dead_code)]
impl Error {
    pub(crate) fn http(err: reqwest::Error, msg: impl Into<String>) -> Error {
        Error::Http {
            msg: msg.into(),
            err,
        }
    }

    pub(crate) fn missing_address_parts<S, P>(s: &str, parts: P) -> Error
    where
        S: Into<String>,
        P: IntoIterator<Item = S>,
    {
        Error::MissingAddressParts {
            address: s.into(),
            parts: parts.into_iter().map(Into::into).collect(),
        }
    }

    pub(crate) fn no_endpoint_available(ty: &str) -> Error {
        Error::NoEndpointAvailable(ty.into())
    }
}
