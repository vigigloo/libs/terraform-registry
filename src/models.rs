use serde::Deserialize;

// ## Modules ##

#[derive(Debug, Clone, Deserialize)]
pub struct Module {
    pub version: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Provider {
    pub version: String,
    #[serde(default)]
    pub protocols: Vec<String>,
    #[serde(default)]
    pub platforms: Vec<ProviderPlatform>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ProviderPlatform {
    pub os: String,
    pub arch: String,
}
