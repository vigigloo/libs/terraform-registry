use clap::{Parser, ValueEnum};
use miette::Result;

#[derive(Debug, Parser)]
struct InspectOptions {
    kind: SourceKind,
    source: String,
}

#[derive(Debug, Clone, Copy, ValueEnum)]
enum SourceKind {
    Module,
    Provider,
}

#[tokio::main]
async fn main() -> Result<()> {
    use terraform_registry::Address;

    setup_logger();

    let opts = InspectOptions::parse();

    let address = match opts.kind {
        SourceKind::Module => Address::parse_module(&opts.source)?,
        SourceKind::Provider => Address::parse_provider(&opts.source)?,
    };

    let registry = address.connect_registry().await?;

    match opts.kind {
        SourceKind::Module => {
            let modules = registry.list_module_versions(&address).await?;
            for module in modules {
                println!("- {}", module.version);
            }
        }
        SourceKind::Provider => {
            let providers = registry.list_provider_versions(&address).await?;
            for provider in providers {
                println!("- {}", provider.version);
                println!("  protocols: {}", provider.protocols.join(", "));
            }
        }
    }

    Ok(())
}

fn setup_logger() {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{filter::LevelFilter, fmt, EnvFilter};

    let fmt_layer = fmt::layer().without_time().with_target(false);

    let filter_layer = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(fmt_layer)
        .init();
}
