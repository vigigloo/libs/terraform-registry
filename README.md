# terraform-registry

[![pipeline status](https://gitlab.com/vigigloo/libs/terraform-registry/badges/master/pipeline.svg)](https://gitlab.com/vigigloo/libs/terraform-registry/-/pipelines)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)
[![Crates.io](https://img.shields.io/crates/v/terraform-registry.svg)](https://crates.io/crates/terraform-registry)
[![docs.rs](https://img.shields.io/docsrs/terraform-registry)](https://docs.rs/terraform-registry)

`terraform-registry` is a wrapper around the [Terraform Registry API](https://developer.hashicorp.com/terraform/registry/api-docs),
allowing querying mainly the [Provider](https://developer.hashicorp.com/terraform/internals/provider-registry-protocol)
and [Module](https://developer.hashicorp.com/terraform/internals/module-registry-protocol) Registry providers APIs.

## Install

Add to your `Cargo.toml`:

```toml
[dependencies]
terraform-registry = "0.1"
```

*MSRV: Rust 1.67*

## Usage

See [`examples/inspect.rs`](./examples/inspect.rs) for usage example.
