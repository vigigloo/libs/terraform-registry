use std::fmt;

use serde::Deserialize;

pub use error::Error;
use error::Result;
pub use models::{Module, Provider, ProviderPlatform};

mod error;
mod models;

const DEFAULT_HOSTNAME: &str = "registry.terraform.io";

/// This crate's entry point.
pub struct Registry {
    client: reqwest::Client,
    endpoints: Endpoints,
}

impl Registry {
    /// Connect to a custom Terraform Registry
    pub async fn connect(hostname: &str) -> Result<Self> {
        let base_url: url::Url = format!("https://{hostname}").parse()?;

        let client = reqwest::Client::builder()
            .build()
            .map_err(|err| Error::http(err, "Failed to create HTTP client"))?;

        let mut registry = Self {
            client,
            endpoints: Endpoints::default(),
        };
        registry.discover(&base_url).await?;
        Ok(registry)
    }

    /// Connect to default Terraform Registry (registry.terraform.io)
    #[inline]
    pub async fn connect_default() -> Result<Self> {
        Self::connect(DEFAULT_HOSTNAME).await
    }

    async fn discover(&mut self, base_url: &url::Url) -> Result<()> {
        use std::collections::HashMap;

        let discover_url = base_url.join("/.well-known/terraform.json")?;

        let res = self
            .client
            .get(discover_url)
            .send()
            .await
            .and_then(|res| res.error_for_status())
            .map_err(|err| Error::http(err, "Failed to discover Terraform registry endpoints"))?;
        let data = res
            .json::<HashMap<String, String>>()
            .await
            .map_err(|err| Error::http(err, "Failed to parse Terraform registry endpoints"))?;

        if let Some(modules_endpoint) = data.get("modules.v1") {
            self.endpoints.modules = Some(base_url.join(modules_endpoint)?);
        }
        if let Some(providers_endpoint) = data.get("providers.v1") {
            self.endpoints.providers = Some(base_url.join(providers_endpoint)?);
        }

        Ok(())
    }

    pub async fn list_module_versions(&self, address: &Address) -> Result<Vec<Module>> {
        #[derive(Deserialize)]
        struct ModuleItem {
            versions: Vec<Module>,
        }
        #[derive(Deserialize)]
        struct ModulesResponse {
            modules: Vec<ModuleItem>,
        }

        let Some(ref endpoint) = self.endpoints.modules else {
            return Err(Error::no_endpoint_available("modules"))
        };
        let Address {
            ref namespace,
            ref kind,
            ..
        } = address;
        let AddressKind::Module { ref name, ref system } = kind else {
            return Err(Error::InvalidAddress(address.clone()));
        };

        let url = endpoint.join(&format!("{namespace}/{name}/{system}/versions"))?;
        let res = self
            .client
            .get(url)
            .send()
            .await
            .and_then(|res| res.error_for_status())
            .map_err(|err| Error::http(err, "Failed to fetch module versions"))?;
        let mut data = res
            .json::<ModulesResponse>()
            .await
            .map_err(|err| Error::http(err, "Failed to parse module versions"))?;

        let item = data
            .modules
            .pop()
            .expect("Empty response, shouldn't happen");
        Ok(item.versions)
    }

    pub async fn list_provider_versions(&self, address: &Address) -> Result<Vec<Provider>> {
        #[derive(Deserialize)]
        struct ProvidersResponse {
            versions: Vec<Provider>,
        }

        let Some(ref endpoint) = self.endpoints.providers else {
            return Err(Error::no_endpoint_available("providers"));
        };
        let Address {
            ref namespace,
            ref kind,
            ..
        } = address;
        let AddressKind::Provider(ref name) = kind else {
            return Err(Error::InvalidAddress(address.clone()));
        };

        let url = endpoint.join(&format!("{namespace}/{name}/versions"))?;
        let res = self
            .client
            .get(url)
            .send()
            .await
            .and_then(|res| res.error_for_status())
            .map_err(|err| Error::http(err, "Failed to fetch provider versions"))?;
        let data = res
            .json::<ProvidersResponse>()
            .await
            .map_err(|err| Error::http(err, "Failed to parse provider versions"))?;

        Ok(data.versions)
    }
}

#[derive(Debug, Default)]
struct Endpoints {
    modules: Option<url::Url>,
    providers: Option<url::Url>,
}

/// Represent a Terraform-formatted address (provider, module, etc.)
#[derive(Debug, Clone)]
pub struct Address {
    pub hostname: String,
    pub namespace: String,
    pub kind: AddressKind,
}

impl Address {
    pub fn parse_module(s: &str) -> Result<Self> {
        let parts = s.split('/').collect::<Vec<_>>();

        match parts.as_slice() {
            [hostname, namespace, name, system] => Ok(Address {
                hostname: hostname.to_string(),
                namespace: namespace.to_string(),
                kind: AddressKind::Module {
                    name: name.to_string(),
                    system: system.to_string(),
                },
            }),
            [namespace, name, system] => Ok(Address {
                hostname: DEFAULT_HOSTNAME.to_string(),
                namespace: namespace.to_string(),
                kind: AddressKind::Module {
                    name: name.to_string(),
                    system: system.to_string(),
                },
            }),
            [_, _] => Err(Error::missing_address_parts(s, ["system"])),
            [_] => Err(Error::missing_address_parts(s, ["name", "system"])),
            _ => Err(Error::missing_address_parts(
                s,
                ["namespace", "name", "system"],
            )),
        }
    }

    pub fn parse_provider(s: &str) -> Result<Self> {
        let parts = s.split('/').collect::<Vec<_>>();

        match parts.as_slice() {
            [hostname, namespace, ty] => Ok(Address {
                hostname: hostname.to_string(),
                namespace: namespace.to_string(),
                kind: AddressKind::Provider(ty.to_string()),
            }),
            [namespace, ty] => Ok(Address {
                hostname: DEFAULT_HOSTNAME.to_string(),
                namespace: namespace.to_string(),
                kind: AddressKind::Provider(ty.to_string()),
            }),
            [_] => Err(Error::missing_address_parts(s, ["type"])),
            _ => Err(Error::missing_address_parts(s, ["namespace", "type"])),
        }
    }

    pub async fn connect_registry(&self) -> Result<Registry> {
        Registry::connect(&self.hostname).await
    }
}

impl fmt::Display for Address {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{hostname}/{namespace}/{kind}",
            hostname = self.hostname,
            namespace = self.namespace,
            kind = self.kind,
        )
    }
}

#[derive(Debug, Clone)]
pub enum AddressKind {
    Module { name: String, system: String },
    Provider(String),
}

impl AddressKind {
    #[inline]
    pub fn module(name: impl Into<String>, system: impl Into<String>) -> Self {
        Self::Module {
            name: name.into(),
            system: system.into(),
        }
    }

    #[inline]
    pub fn provider(name: impl Into<String>) -> Self {
        Self::Provider(name.into())
    }
}

impl fmt::Display for AddressKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            AddressKind::Module {
                ref name,
                ref system,
            } => write!(f, "{name}/{system}"),
            AddressKind::Provider(ref name) => name.fmt(f),
        }
    }
}
